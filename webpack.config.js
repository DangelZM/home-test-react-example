var webpack = require('webpack');
var path = require('path');

config = {
    entry: [
      'webpack/hot/dev-server',
      path.resolve(__dirname, 'js/app.js')
    ],
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js',
    },
    module: {
      loaders: [
          { test: /\.js?$/, loaders: ['react-hot', 'babel?presets[]=es2015&presets[]=react'], exclude: /node_modules/ },
          { test: /\.css$/, loader: "style!css" }
      ]
    }
};

module.exports = config;
