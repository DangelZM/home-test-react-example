import React from 'react';
import Nav from './Nav';

const styles = {};
styles.content = {
  background: "#efefef",
}

class App extends React.Component {
  render() {
    return (
      <div className="layout horizontal fit">
        <Nav className="flex-basic" />
        <div className="flex" style={styles.content}>
          {this.props.children}
        </div>
      </div>
    )
  }
}

module.exports = App
