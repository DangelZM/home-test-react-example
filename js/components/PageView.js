import React from 'react';

class PageView extends React.Component {
  render() {
    return (
        <div>
          {this.props.children}
        </div>
    )
  }
}

module.exports = PageView
