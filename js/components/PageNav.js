import React from 'react';
import { Link } from 'react-router';

const dark = '#369';
const light = '#fff';
const styles = {};

styles.wrapper = {
  overflow: 'hidden',
  color: light,
  height: 40
}

styles.link = {
  display: 'inline-block',
  float: 'left',
  padding: '0 10px',
  height: 40,
  width: 150,
  lineHeight: '40px',
  textAlign: 'center',
  fontWeight: 200,
  textDecoration: 'none',
  border: '1px solid #369',
  borderRadius: '8px 8px 0 0',
  color: dark
}

styles.activeLink = {
  display: 'inline-block',
  float: 'left',
  padding: '0 10px',
  height: 40,
  width: 150,
  lineHeight: '40px',
  textAlign: 'center',
  fontWeight: 200,
  textDecoration: 'none',
  border: '1px solid #369',
  borderRadius: '8px 8px 0 0',
  background: dark,
  color: light
}

class PageNav extends React.Component {

  render() {
    let tabs = this.props.tabs;
    let displayTabs = Object.keys(tabs).map((k) => tabs[k]);
    return (
      <div style={styles.wrapper}>
          {displayTabs.map(function(tab) {
              return (
                  <Link key={tab.name} to={tab.url} style={styles.link} activeStyle={styles.activeLink}>{tab.name}</Link>
              );
          })}
      </div>
    )
  }
}

export default PageNav
