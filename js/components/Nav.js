import React from 'react';
import { Link } from 'react-router';

const dark = 'hsl(200, 20%, 20%)';
const light = '#fff';
const styles = {};

styles.wrapper = {
  width: '250px',
  padding: '10px',
  overflow: 'hidden',
  background: dark,
  color: light
}

styles.link = {
  height: '30px',
  lineHeight: '30px',
  padding: 11,
  color: light,
  fontWeight: 200,
  textDecoration: 'none'
}

styles.activeLink = {
  padding: 11,
  fontWeight: 200,
  background: light,
  color: dark
}

class Nav extends React.Component {
  render() {

    return (
      <div style={styles.wrapper}>
        <div className="layout vertical">
          <Link to="/products" style={styles.link} activeStyle={styles.activeLink}>Products</Link>{' '}
          <Link to="/customers" style={styles.link} activeStyle={styles.activeLink}>Customers</Link>{' '}
        </div>
      </div>
    )
  }
}

export default Nav
