import React from 'react';

const styles = {};

styles.header = {
  height: '30px',
  lineHeight: '30px'
}

styles.content = {
  padding: 10
}

class CollapseBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapseId: props.collapseId,
      collapsed: props.collapsed
    };
  }

  collapseClick() {
    let newState = {
      collapseId: this.state.collapseId,
      collapsed: !this.state.collapsed
    };

    this.props.changeState(newState);
  }

  render() {
    console.log('Render CollapseBox', this.state.collapseId, this.state.collapsed);
    let collapseButton = this.state.collapsed ? "\u25B2" : "\u25BC";

    return (
      <div>
          <div style={styles.header}>
            HeaderText
            <span onClick={this.collapseClick.bind(this)}>{collapseButton}</span>
          </div>
          <div style={styles.content}>
              Content
          </div>
      </div>
    )
  }
}

module.exports = CollapseBox
