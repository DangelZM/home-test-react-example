import React from 'react';
import { Router, Route, IndexRedirect } from 'react-router';
import { createHistory, useBasename } from 'history';

const history = useBasename(createHistory)({
    basename: '/'
});

var App = require('./components/App');
var NotFound = require('./components/NotFound');
var PageView = require('./components/PageView');
var Products = require('./pages/Products');
var Customers = require('./pages/Customers');

module.exports = (
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRedirect to="products" />
        <Route path="products" component={PageView} >
          <IndexRedirect to="general" />
          <Route path=":tab" component={Products} />
        </Route>
        <Route path="customers" component={PageView}>
          <IndexRedirect to="general" />
          <Route path=":tab" component={Customers} />
        </Route>
      </Route>
      <Route path="/404" component={NotFound}/>
      <Route path="*" component={NotFound}/>
    </Router>
)
