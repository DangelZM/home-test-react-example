import React from 'react';
import PageNav from '../../components/PageNav';

var tabs = {
  general: {name:"General", url: "/products/general"},
  desc: {name:"Description", url: "/products/desc"}
};

class Products extends React.Component {
  render() {
    let { location, params, history } = this.props;

    if(!tabs[params.tab]){
      history.replace('/404');
    }

    let Content = require('./tabs/' + params.tab);

    return (
      <div>
          <PageNav tabs={tabs} />
          <Content {...this.props} />
      </div>
    )
  }
}

module.exports = Products
