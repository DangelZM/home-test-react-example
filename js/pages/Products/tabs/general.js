import React from 'react';
import CollapseBox from '../../../components/CollapseBox';


class GeneralTab extends React.Component {
  constructor(props) {
    super(props);
    let initState = {
      collapse: {
        1: false,
        2: false,
        3: false
      }
    };
    let checkedState = (() => {
      let newState;
      let query = props.location.query;

      if(query.collapse){
        let newCollapse = query.collapse.split(',');
        newState = Object.keys(initState.collapse).map(k => {
          initState.collapse[k] = (newCollapse.indexOf(k) !== -1);
        });
      }
      console.log('newState ', initState);

      return initState;
    })();

    this.state = checkedState;
  }

  changeState(newState) {
    console.log(newState);
    let newCollapse = this.state.collapse;
    newCollapse[newState['collapseId']] = newState["collapsed"];

    console.log(newCollapse);

    this.setState({
      collapse: newCollapse
    })
  }

  render() {
    console.log('Render collapse ', this.state.collapse);
    let list = this.state.collapse;
    return (
      <div>
          <h3>GeneralTab</h3>
          <CollapseBox changeState={this.changeState.bind(this)} collapseId="1" collapsed={list[1]} />
          <CollapseBox changeState={this.changeState.bind(this)} collapseId="2" collapsed={list[2]} />
          <CollapseBox changeState={this.changeState.bind(this)} collapseId="3" collapsed={list[3]} />
      </div>
    )
  }
}

module.exports = GeneralTab
