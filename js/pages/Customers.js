import React from 'react';

class Customers extends React.Component {
  render() {
    return (
      <div>
        <h1>Customers</h1>
        <div style={{ padding: 20 }}>
          {this.props.children}
        </div>
      </div>
    )
  }
}

module.exports = Customers
