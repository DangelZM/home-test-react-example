# Home Test Project with React

## To start work with project

You need node.js and npm installed.  
Run in your terminal `npm install` to get dependencies.  

Server for development: run `npm run dev` will be exist in url http://localhost:8080  
Prepare code for production: run `npm run deploy`  

## Run code on production

You can use `server.js` like example of server for static file, or you can use Nginx server.
All you need are files in `dist` folder.

So, to get result just run `node server.js`  and open in your browser link http://localhost:3016

Have a good day! :)
